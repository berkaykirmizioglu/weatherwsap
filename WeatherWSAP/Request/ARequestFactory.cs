﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMonitor.Request
{
    public abstract class ARequestFactory : AClientFactory
    {
        AEnviroment enviroment = new AEnviroment();

        public RestRequest Create(string url, Method method)
        {
            return new RestRequest(url + "&APPID=" + enviroment.apiKey , method);
        }

        public IRestResponse ExecuteRequest(RestRequest request) 
        {
            IRestResponse response = GetClient().Execute(request);

            return response;
        }
    }
}
