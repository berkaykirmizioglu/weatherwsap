﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServiceMonitor.Entity;
using NUnit.Framework;

namespace ServiceMonitor.Request
{
    public class CurrentWeatherRequest : ARequestFactory
    {
        public IRestResponse GetByName(string name)
        {
            var request = Create(string.Format("weather?q={0}", name), Method.GET);
            
            var response = ExecuteRequest(request);

            return response;
        }
    }
}
