﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMonitor
{
    public abstract class AClientFactory
    {
        AEnviroment enviroment = new AEnviroment();

        public RestClient GetClient()
        {
            var client = new RestClient(enviroment.baseURL);

            return client;
        }
    }
}
