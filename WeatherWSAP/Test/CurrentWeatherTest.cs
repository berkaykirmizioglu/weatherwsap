﻿using Newtonsoft.Json;
using NUnit.Framework;
using ServiceMonitor.Entity;
using ServiceMonitor.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMonitor.Test
{
    [TestFixture]
    [Parallelizable]
    class CurrentWeatherTest : CurrentWeatherRequest
    {
        [Test]
        [TestCase("London", TestName = "CurrentWeatherTest")]
        public void CurrentWeatherTest_(params string[] testCaseSource)
        {
            var currentWeatherDTO = JsonConvert.DeserializeObject<CurrentWeatherDTO>(GetByName(testCaseSource[0]).Content);

            Assert.AreEqual("London", currentWeatherDTO.name);
        }
    }
}
