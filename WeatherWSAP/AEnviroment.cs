﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMonitor
{
    public class AEnviroment
    {
        public string enviroment { get; set; }
        public string apiKey { get; set; }
        public string baseURL { get; set; }

        public AEnviroment()
        {
            baseURL = getBaseURL();
            apiKey = getApiKey();
        }

        private string getBaseURL() 
        {
            enviroment = ConfigurationSettings.AppSettings["enviroment"];

            switch (enviroment)
            {
                case "preprod":
                    baseURL = ConfigurationSettings.AppSettings["baseURLPreprod"];
                    break;
                case "prod":
                    baseURL = ConfigurationSettings.AppSettings["baseURLProd"];
                    break;
            }

            return baseURL;
        }

        private string getApiKey()
        {
            enviroment = ConfigurationSettings.AppSettings["enviroment"];

            switch (enviroment)
            {
                case "preprod":
                    apiKey = ConfigurationSettings.AppSettings["apiKeyPreprod"];
                    break;
                case "prod":
                    apiKey = ConfigurationSettings.AppSettings["apiKeyProd"];
                    break;
            }

            return apiKey;
        }
    }
}
