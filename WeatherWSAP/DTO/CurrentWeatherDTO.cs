﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMonitor.Entity
{
    public class CurrentWeatherDTO
    {
        [DeserializeAs(Name = "name")]
        public string name { get; set; }
    }
}
